# run minikube
minikube start

eval $(minikube -p minikube docker-env)

# run metallb
minikube addons enable metallb
kubectl apply -f ./srcs/metallb.yaml

# run nginx
docker build -t nginx ./srcs/nginx/
kubectl apply -f ./srcs/nginx/nginx.yaml

# run mysql
docker build -t mysql ./srcs/mysql/
kubectl apply -f ./srcs/mysql/mysql.yaml

# run phpmyadmin
docker build -t phpmyadmin ./srcs/phpmyadmin/
kubectl apply -f ./srcs/phpmyadmin/phpmyadmin.yaml

# run wordpress
docker build -t wordpress ./srcs/wordpress/
kubectl apply -f ./srcs/wordpress/wordpress.yaml

# run influxdb
docker build -t influxdb ./srcs/influxdb
kubectl apply -f ./srcs/influxdb/influxdb.yaml

# run telegraf
docker build -t telegraf ./srcs/telegraf
kubectl apply -f ./srcs/telegraf/telegraf.yaml

# run grafana
docker build -t grafana ./srcs/grafana
kubectl apply -f ./srcs/grafana/grafana.yaml

# run ftps
docker build -t ftps ./srcs/ftps
kubectl apply -f ./srcs/ftps/ftps.yaml

minikube dashboard

# check
# kubectl exec deploy/ftps -- pkill vsftpd
# kubectl exec deploy/grafana -- pkill grafana
# kubectl exec deploy/telegraf -- pkill telegraf
# kubectl exec deploy/influxdb -- pkill influx
# kubectl exec deploy/wordpress -- pkill nginx
# kubectl exec deploy/wordpress -- pkill php-fpm7
# kubectl exec deploy/phpmyadmin -- pkill nginx
# kubectl exec deploy/phpmyadmin -- pkill php-fpm7
# kubectl exec deploy/mysql -- pkill /usr/bin/mysqld 
# kubectl exec deploy/nginx -- pkill nginx
# kubectl exec deploy/nginx -- pkill sshd

# ssh -p 22 gmegga@192.168.99.201

